import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import bgblue from '../assets/images/bgblue.png';
import style from '../assets/module-css/style.module.css';

const navBar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-warna fixed-top" style={{ paddingLeft: '2%', paddingRight: '10%', backgroundColor: '#F1F3FF' }}>
            <div className="container-fluid">
                <a className="navbar-brand" href="/"><img className="mx-5" src={bgblue} alt="logo" style={{ width: '40%' }} /></a>

                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse navbar-menu" id="navbarNavDropdown" style={{ display:'flex', justifyContent:'flex-end' }}>
                    <ul className="navbar-nav">
                        <li className={style.navitem}>
                            <a className="nav-link active" aria-current="page" href="#ourservices">Our Services</a>
                        </li>&nbsp;&nbsp;&nbsp;
                        <li className={style.navitem}>
                            <a className="nav-link active" href="#whyus">Why Us</a>
                        </li>&nbsp;&nbsp;&nbsp;
                        <li className={style.navitem}>
                            <a className="nav-link active" href="#testi">Testimonial</a>
                        </li>&nbsp;&nbsp;&nbsp;
                        <li className={style.navitem}>
                            <a className="nav-link active" href="#faq">FAQ</a>
                        </li> &nbsp;&nbsp;&nbsp;&nbsp;

                        <button className="btn btn-success" type="submit">Register</button>

                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default navBar