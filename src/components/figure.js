import React from "react";

export default function Figure(props) {
    const { figure } = props;

    return (
        <div className="card" style={{ width: '23rem', margin: '1rem' }}>
            <img src={figure.car_image} className="card-img-top" alt="..." />
            <div className="card-body">
                <h5 className="card-text">
                    {figure.car_name}/{figure.car_type}
                </h5>
                <h4 className="card-title">
                    {figure.rent_cost} / hari
                </h4><br />
                <p className="card-text"><i className='bx bx-time-five'></i>
                    {figure.updatedAt}
                </p>
            </div>
        </div>
    )
}