import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import style from '../assets/module-css/style.module.css';
import bgblue from '../assets/images/bgblue.png';
import icon_facebook from '../assets/img/icon_facebook.svg';
import icon_instagram from '../assets/img/icon_instagram.svg';
import icon_twitter from '../assets/img/icon_twitter.svg';
import icon_mail from '../assets/img/icon_mail.svg';
import icon_twitch from '../assets/img/icon_twitch.svg';

const Footer = () => {
    return (
        <footer className="container-fluid"><div className={style.itemsfooter}>
            <div className={style.item}>
                <p className="mb-4">Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                <p className="mb-4">binarcarrental@gmail.com</p>
                <p className="mb-4">081-233-334-808</p>
            </div>
            <div className={style.item}>
                <a className="nav-link mb-2 font-weight-bold" style={{ color:'black' }} href="#ourservices">Our Services</a>
                <a className="nav-link mb-2 font-weight-bold" style={{ color:'black' }} href="#whyus">Why Us</a>
                <a className="nav-link mb-2 font-weight-bold" style={{ color:'black' }} href="#testi">Testimonial</a>
                <a className="nav-link mb-2 font-weight-bold" style={{ color:'black' }} href="#faq">FAQ</a>
            </div>
            <div className={style.item}>
                <p className="mb-4">Connect with us</p>
                <img src={icon_facebook} alt="facebook" />
                <img src={icon_instagram} alt="instagram" />
                <img src={icon_twitter} alt="twitter" />
                <img src={icon_mail}alt="mail" />
                <img src={icon_twitch} alt="twitch" />
            </div>
            <div className={style.item}>
                <p className="mb-4">Copyright Binar 2022</p>
                <a className="" href="/"><img src={bgblue} style={{ width:'40%' }} alt="" /></a>
            </div>
        </div></footer>
    )
}

export default Footer;