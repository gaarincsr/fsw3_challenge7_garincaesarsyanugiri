//   all ------------------
function initParadoxWay() {
    "use strict";
   
    if ($(".testimonialscarousel").length > 0) {
        var j2 = new Swiper(".testimonialscarousel .swipercontainer", {
            preloadImages: false,
            slidesPerView: 1,
            spaceBetween: 20,
            loop: true,
            grabCursor: true,
            mousewheel: false,
            centeredSlides: true,
            pagination: {
                el: '.tcpagination',
                clickable: true,
                dynamicBullets: true,
            },
            navigation: {
                nextEl: '.listingcarouselbuttonnext',
                prevEl: '.listingcarouselbuttonprev',
            },
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                },
                
            }
        });
    }
    
// bubbles -----------------
    
    
    setInterval(function () {
        var size = randomValue(sArray);
        $('.bubbles').append('<div class="individual-bubble" style="left: ' + randomValue(bArray) + 'px; width: ' + size + 'px; height:' + size + 'px;"></div>');
        $('.individual-bubble').animate({
            'bottom': '100%',
            'opacity': '-=0.7'
        }, 4000, function () {
            $(this).remove()
        });
    }, 350);
    
}

//   Init All ------------------
$(document).ready(function () {
    initParadoxWay();
});