import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { Home, Cars } from './pages';
import NavBar from './components/NavigationBar';
import Foooter from './components/Footer';
import './App.css';
import store from './store';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <NavBar />
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route path='/cars' element={<Cars />} />
        </Routes>
        <Foooter />
      </Provider>
    </BrowserRouter>
  );
}

export default App;
