import { combineReducers } from 'redux';
import figureReducer from './figureReducer';

export default combineReducers({
    figureReducer,
})