const initialState = {
    figures: [],
}

export default function figureReducer (state = initialState, action) {
    switch (action.type) {
        case 'FETCH_FIGURES':
            return {...state, figures: action.payload};
        default:
            return state;
    }
}