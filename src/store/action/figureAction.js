export function fetchFigureAction() {
    return (dispatch) => {
        fetch('http://localhost:8080/api/v1/')
            .then((response) => {
                if (response.ok) {
                    return response.json()
                } else {
                    throw response.statusText
                }
            })
            .then((data) => {
                dispatch({
                    type: 'FETCH_FIGURES',
                    payload: data.data
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }
}