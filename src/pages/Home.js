import React from "react";
import carSVG from '../assets/images/img_car.svg';
import imgService from '../assets/img/img_service.png';
import listtesti from '../assets/images/listtesti.png';
import style from '../assets/module-css/style.module.css';
function Home() {
    return (
        <body>
            <section id="Home" className="container-fluid" style={{ backgroundColor: '#F1F3FF' }}>
                <div className={style.itemsdiv}>
                    <div className={style.item}>
                        <h1 style={{ fontWeight: '700' }}>Sewa dan Rental Mobil Terbaik<br />di kawasan (Lokasimu)</h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas<br />
                            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu<br />
                            untuk sewa mobil selama 24 jam.
                        </p>
                        <a href="/cars"><button className="btn btn-success" type="button">Mulai Sewa Mobil</button></a>
                    </div>
                    <div className={style.item}>
                        <img className={style.imgheader} src={carSVG} alt="" />
                    </div>
                </div>
            </section>

            <section id="ourservices" className="container-fluid" style={{ backgroundColor: 'white', paddingTop: '10%' }}>
                <div className={style.itemsdiv}>
                    <div className={style.item}>
                        <img src={imgService} alt="" />
                    </div>
                    <div className={style.item}>
                        <h1>Best Car Rental for any kind of trip in<br />
                            Lokasimu!
                        </h1>
                        <p>Sewa mobil di Lokasimu bersama Binar Car Rental jaminan harga lebih<br />
                            murah dibandingkan yang lain, kondisi mobil baru, serta kualitas<br />
                            pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
                        </p>

                        <div className={style.subitem}>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                                <path d="M17.3333 8L9.99996 15.3333L6.66663 12" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg> &nbsp; &nbsp;
                            <p>Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                        </div>

                        <div className={style.subitem}>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                                <path d="M17.3333 8L9.99996 15.3333L6.66663 12" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg> &nbsp; &nbsp;
                            <p>Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                        </div>

                        <div className={style.subitem}>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                                <path d="M17.3333 8L9.99996 15.3333L6.66663 12" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg> &nbsp; &nbsp;
                            <p>Sewa Mobil Jangka Panjang Bulanan</p>
                        </div>

                        <div className={style.subitem}>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                                <path d="M17.3333 8L9.99996 15.3333L6.66663 12" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg> &nbsp; &nbsp;
                            <p>Gratis Antar - Jemput Mobil di Bandara</p>
                        </div>

                        <div className={style.subitem}>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="12" cy="12" r="12" fill="#CFD4ED" />
                                <path d="M17.3333 8L9.99996 15.3333L6.66663 12" stroke="#0D28A6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                            </svg> &nbsp; &nbsp;
                            <p>Layanan Airport Transfer / Drop In Out</p>
                        </div>
                    </div>
                </div>
            </section>


            <section id="whyus" className="container-fluid" style={{ backgroundColor: '', paddingTop: '10%' }}>
                <div className="container">
                    <div className="row">
                        <h1><strong>Why Us?</strong></h1> <br />
                        <p>Mengapa harus pilih Binar Car Rental?</p>
                    </div>
                    <div className="row">

                        <div className="col">
                            <div className="card p-4 px-2 postition-absolute" style={{ width: '18rem' }}>
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="16" cy="16" r="16" fill="#F9CC00" />
                                    <path d="M11.8333 24.3333H9.33329C8.89127 24.3333 8.46734 24.1577 8.15478 23.8452C7.84222 23.5326 7.66663 23.1087 7.66663 22.6667V16.8333C7.66663 16.3913 7.84222 15.9674 8.15478 15.6548C8.46734 15.3423 8.89127 15.1667 9.33329 15.1667H11.8333M17.6666 13.5V10.1667C17.6666 9.50362 17.4032 8.86773 16.9344 8.39889C16.4656 7.93005 15.8297 7.66666 15.1666 7.66666L11.8333 15.1667V24.3333H21.2333C21.6352 24.3379 22.0253 24.197 22.3315 23.9367C22.6378 23.6763 22.8397 23.3141 22.9 22.9167L24.05 15.4167C24.0862 15.1778 24.0701 14.9339 24.0027 14.7019C23.9354 14.4698 23.8184 14.2552 23.6598 14.0729C23.5013 13.8906 23.305 13.7449 23.0846 13.646C22.8642 13.5471 22.6249 13.4973 22.3833 13.5H17.6666Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>

                                <div className="card-body">
                                    <h3 className="card-text">Mobil Lengkap</h3>
                                    <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card p-4 px-2" style={{ width: '18rem' }}>
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="16" cy="16" r="16" fill="#FA2C5A" />
                                    <path d="M23.1583 17.175L17.1833 23.15C17.0285 23.305 16.8447 23.4279 16.6424 23.5118C16.44 23.5956 16.2232 23.6388 16.0041 23.6388C15.7851 23.6388 15.5682 23.5956 15.3659 23.5118C15.1636 23.4279 14.9797 23.305 14.825 23.15L7.66663 16V7.66666H16L23.1583 14.825C23.4687 15.1373 23.6429 15.5597 23.6429 16C23.6429 16.4403 23.4687 16.8627 23.1583 17.175V17.175Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M11.8334 11.8333H11.8417" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>

                                <div className="card-body">
                                    <h3 className="card-text">Harga Murah</h3>
                                    <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card p-4 px-2" style={{ width: '18rem' }}>
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="16" cy="16" r="16" fill="#0D28A6" />
                                    <path d="M16 24.3333C20.6023 24.3333 24.3333 20.6024 24.3333 16C24.3333 11.3976 20.6023 7.66666 16 7.66666C11.3976 7.66666 7.66663 11.3976 7.66663 16C7.66663 20.6024 11.3976 24.3333 16 24.3333Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M16 11V16L19.3333 17.6667" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>

                                <div className="card-body">
                                    <h3 className="card-text">Layanan 24 Jam</h3>
                                    <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card p-4 px-2" style={{ width: '18rem' }}>
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="16" cy="16" r="16" fill="#5CB85F" />
                                    <path d="M16.0001 18.5C19.2217 18.5 21.8334 15.8883 21.8334 12.6667C21.8334 9.44502 19.2217 6.83334 16.0001 6.83334C12.7784 6.83334 10.1667 9.44502 10.1667 12.6667C10.1667 15.8883 12.7784 18.5 16.0001 18.5Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M12.8416 17.575L11.8333 25.1667L15.9999 22.6667L20.1666 25.1667L19.1583 17.5667" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>

                                <div className="card-body">
                                    <h3 className="card-text">Sopir Profesional</h3>
                                    <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="testi" className="container-fluid" style={{ width: '100%', paddingTop: '5%' }}>
                <div className="mx-auto d-block text-center">
                    <h1>Testimonial</h1>
                    <p>Berbagai review positif dari para pelanggan kami</p>
                </div>
                <div className="" style={{ width: '100%' }}>
                    <img style={{ width: '100%' }} src={listtesti} alt="" /><br /><br />
                </div>

                <div className="d-flex justify-content-center">
                    <button className="border border-white border-0" style={{ backgroundColor: 'transparent' }} type="button"><img style={{ width: '70px' }} src="icon/Left_button.svg" alt="" /></button> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <button className="border border-white border-0" style={{ backgroundColor: 'transparent' }} type="button"><img style={{ width: '70px' }} src="icon/Right_button.svg" alt="" /></button>
                </div>
            </section>


            <div className="container-fluid">
                <div className="container rounded-3 d-block" style={{ marginLeft:'15%' }}><div className={style.containerfaq}>
                    <center><h1 style={{ fontWeight:'700' }}>Sewa Mobil di (Lokasimu) Sekarang</h1><br />
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br />tempor incididunt ut labore et dolore magna aliqua. </p></center><br />
                    <center><a href="/cars"><button type="button" className="btn" style={{ margin: 'auto' }}>Mulai Sewa Mobil</button></a></center>
                </div></div>
            </div>
            <section id="faq">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <h2 style={{ fontWeight:'700' }}>Frequently Asked Question</h2><br />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>

                        <div className="col">
                            <div className="accordion accordion-flush" id="accordionFlushExample">
                                <div className="accordion-item border border-dark">
                                    <h2 className="accordion-header" id="flush-headingOne">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                            Apa saja syarat yang dibutuhkan?
                                        </button>
                                    </h2>
                                    <div id="flush-collapseOne" className="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                        <div className="accordion-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit facilis adipisci repellendus hic. Repudiandae ad suscipit fuga quasi laudantium veniam nobis adipisci! Molestias, tenetur nam. Voluptate laboriosam optio quibusdam tenetur.</div>
                                    </div>
                                </div>

                                <br />

                                <div className="accordion-item border border-dark">
                                    <h2 className="accordion-header" id="flush-headingTwo">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                            Berapa hari minimal sewa mobil lepas kunci?
                                        </button>
                                    </h2>
                                    <div id="flush-collapseTwo" className="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                        <div className="accordion-body">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Officiis dolorum labore ut nesciunt facilis aperiam, necessitatibus aliquid ipsam consectetur suscipit provident praesentium odit totam quam, alias tempora error quibusdam est!</div>
                                    </div>
                                </div>

                                <br />

                                <div className="accordion-item border border-dark">
                                    <h2 className="accordion-header" id="flush-headingThree">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                            Berapa hari sebelumnya sabaiknya booking sewa mobil?
                                        </button>
                                    </h2>
                                    <div id="flush-collapseThree" className="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                        <div className="accordion-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet ducimus rem culpa facere ex laudantium saepe totam est tempore? Itaque et iusto voluptas mollitia voluptatem obcaecati quas praesentium tenetur ipsam?</div>
                                    </div>
                                </div>

                                <br />

                                <div className="accordion-item border border-dark">
                                    <h2 className="accordion-header" id="flush-heading4">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse4" aria-expanded="false" aria-controls="flush-collapse4">
                                            Apakah Ada biaya antar-jemput?
                                        </button>
                                    </h2>
                                    <div id="flush-collapse4" className="accordion-collapse collapse" aria-labelledby="flush-heading4" data-bs-parent="#accordionFlushExample">
                                        <div className="accordion-body"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque quos veniam veritatis sequi magni reprehenderit voluptate ex tempora, soluta tempore molestias, dolores possimus eaque fuga odio et eum ipsam accusantium.</div>
                                    </div>
                                </div>
                                <br />

                                <div className="accordion-item border border-dark">
                                    <h2 className="accordion-header" id="flush-heading5">
                                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse5" aria-expanded="false" aria-controls="flush-collapse5">
                                            Bagaimana jika terjadi kecelakaan
                                        </button>
                                    </h2>
                                    <div id="flush-collapse5" className="accordion-collapse collapse" aria-labelledby="flush-heading5" data-bs-parent="#accordionFlushExample">
                                        <div className="accordion-body">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsam laborum nostrum magnam magni harum nesciunt voluptate saepe cumque sit beatae, officiis esse dolore reprehenderit quo commodi. Eos quam maxime iusto!</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>
        </body>
    )
}
export default Home