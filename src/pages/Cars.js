import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { fetchFigureAction } from '../store/action/figureAction'
import Figure from '../components/figure';
import style from '../assets/module-css/style.module.css';
import carSVG from '../assets/images/img_car.svg';

// import axios from 'axios';

function Cars() {

    const figures = useSelector((state) => state.figureReducer.figures);
    const dispatch = useDispatch();
    const [ category, setCategory ] = useState('all');

    function handleSmall() {
        setCategory('Small');
    }
    function handleMed() {
        setCategory('Medium');
    }
    function handleLarge() {
        setCategory('Large');
    }
    function handleAll() {
        setCategory('all');
    }

    let fetchCategory;
    if (category === 'all') {
        fetchCategory = figures;
    } else {
        fetchCategory = figures.filter((figure) => figure.car_type === category);
    }

    useEffect(() => {
        dispatch(fetchFigureAction())
    }, [])

    return (
        <body>
            <section id="Home" className="container-fluid" style={{ backgroundColor: '#F1F3FF' }}>
                <div className={style.itemsdiv}>
                    <div className={style.item}>
                        <h1 style={{ fontWeight: '700' }}>Sewa dan Rental Mobil Terbaik<br />di kawasan (Lokasimu)</h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas<br />
                            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu<br />
                            untuk sewa mobil selama 24 jam.
                        </p>
                    </div>
                    <div className={style.item}>
                        <img className={style.imgheader} src={carSVG} alt="" />
                    </div>
                </div>
            </section>


            <div className="mx-5 py-4" style={{ border: '2px solid blue', borderRadius:'25px', backgroundColor:'#F1F3FF' }}>
                <div className={style.itemsdiv}>
                    <div className={style.item}>
                        <p className="text-filter">Tipe Kendaraan</p>
                        <div className="dropdown">
                            <a className="btn dropdown-toggle" href="/cars" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false" style={{ border: '1px' }}>
                                Pilih Tipe Kendaraan
                            </a>

                            <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <li><button onClick={() => handleAll()} className="dropdown-item">All</button></li>
                                <li><button onClick={() => handleSmall()} className="dropdown-item" >Small</button></li>
                                <li><button onClick={() => handleMed()} className="dropdown-item" >Medium</button></li>
                                <li><button onClick={() => handleLarge()} className="dropdown-item" >Large</button></li>
                            </ul>
                        </div>
                    </div>
                    <div className={style.item}>
                        <p className="text-filter">Tanggal</p>
                        <input placeholder="Pilih Tanggal" type="text"
                            onfocus="(this.type='date')" id="date" className="box-input" disabled />
                    </div>
                    <div className={style.item}>
                        <p className="text-filter">Waktu Jemput/Ambil</p>
                        <input placeholder="Pilih Waktu" type="text"
                            onfocus="(this.type='time')" id="time" className="box-input" disabled />
                    </div>
                    <div className={style.item}>
                        <p className="text-filter">Jumlah Penumpang(opsional)</p>
                        <input type="number" id="capacity" placeholder="Jumlah Penumpang" className="box-input" disabled />
                    </div>
                    {/* <div className={style.item}>
                        <button id="filter-btn" className="btn btn-success">Cari Mobil</button>
                    </div> */}
                </div>
            </div> <br /><br />

            <div className="d-flex flex-row flex-wrap justify-content-center">
                {fetchCategory.map((figure) => (
                        <Figure key={figure.id} figure={figure} />
                    ))}
            </div>


        </body>
    )
}

export default Cars;