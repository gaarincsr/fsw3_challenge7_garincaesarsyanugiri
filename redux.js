const redux = require('redux');

const initialState = {
    count: 0,
    name: 'Garin',
}

const rootReducer = (state = initialState, action) => {
    switch (action. type) {
        case 'ADD_COUNT':
            return {
                ...state,
                count: state.count + 1,
            }
        case 'CHANGE_NAME':
            return {
                ...state,
                name: action.newValue,
            }

        default:
            return state;
    }
}

const store = redux.createStore(rootReducer);
console.log(store.getState());

store.dispatch({
    type: 'ADD_COUNT'
})
store.dispatch({
    type: 'CHANGE_NAME',
    newValue: 'BINAR',
})
console.log(store.getState());
